package com.example.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {

    @RequestMapping(value = {"/", "/home/", "/index"})    
    public String index(Model model) {
    	model.addAttribute("datetime", new Date());
    	model.addAttribute("username", "Employee");
    	
    	return "index";
    }
}