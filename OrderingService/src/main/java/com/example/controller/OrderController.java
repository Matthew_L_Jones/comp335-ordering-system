package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.orderingSystem.MovieOrder;
import com.example.orderingSystem.MovieRepository;

@RestController
@RequestMapping(value = "/orders")
public class OrderController {
	
	private MovieRepository movieRepository;
	
	@Autowired
	public OrderController(MovieRepository movieRepository){
		this.movieRepository = movieRepository;
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<MovieOrder> getAll(){
		return movieRepository.findAll();
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public List<MovieOrder> create(@RequestBody MovieOrder movieOrder){
		movieRepository.save(movieOrder);
		
		return movieRepository.findAll();
	}	
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public List<MovieOrder> remove(@PathVariable long id){
		movieRepository.deleteById(id);
		
		return movieRepository.findAll();
	}		
}
