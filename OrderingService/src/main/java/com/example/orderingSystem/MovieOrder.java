//instruction from https://www.youtube.com/watch?v=C3bNEz9opuU
package com.example.orderingSystem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MovieOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;	
	
	private String movieName;	
	private double pricePerChild;
	private double pricePerStudent;
	private double pricePerAdult;		
	
	public MovieOrder() {}
	
	public MovieOrder(String movieName, double pricePerChild, double pricePerStudent, double pricePerAdult) {
		this.movieName = movieName;
		this.pricePerChild = pricePerChild;
		this.pricePerStudent = pricePerStudent;
		this.pricePerAdult = pricePerAdult;
	}
	
	public long getId() {
		return id;
	}
	
	public String getMovieName() {
		return movieName;
	}
	
	public double getPricePerChild() {
		return pricePerChild;
	}
	
	public double getPricePerStudent() {
		return pricePerStudent;
	}
	
	public double getPricePerAdult() {
		return pricePerAdult;
	}
}
