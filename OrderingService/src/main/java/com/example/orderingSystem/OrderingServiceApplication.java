package com.example.orderingSystem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.example.controller")
@EnableEurekaClient

public class OrderingServiceApplication{
	public static void main(String[] args) {
		SpringApplication.run(OrderingServiceApplication.class, args);
	}
}
