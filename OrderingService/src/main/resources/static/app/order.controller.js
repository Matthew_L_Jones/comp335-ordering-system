(function () {
    'use strict';

    angular
        .module('app')
        .controller('OrderController', OrderController);

    OrderController.$inject = ['$http'];

    function OrderController($http) {
        var vm = this;

        vm.movies = [];
        vm.getAll = getAll;
        vm.deleteMovie = deleteMovie;

        init();

        function init(){
            getAll();
        }

        function getAll(){
            var url = "/orders/all";
            var moviesPromise = $http.get(url);
            moviesPromise.then(function(response){
                vm.movies = response.data;
            });
        }

        function deleteMovie(id){
            var url = "/orders/delete/" + id;
            $http.post(url).then(function(response){
                vm.movies = response.data;
            });
        }
    }
})();